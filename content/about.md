---
title: "About"
date: 2018-08-16T21:05:40+01:00
draft: false
---

Hi, I'm Greg a recent graduate of Abersywyth University and now currently doing all things DevOps at [DevOpsGroup](https://devopsgroup.com), Cardiff. My current position means I'm working with clients to develop their software, create/maintance infrastructure or creating a deployment pipelines all while working in an Agile way (usually Scrum or Kanban). Experienced in working with Puppet, Ansible, Terraform, Jenkins, Amazon Web Services, Azure and more...

I'm also a keen photographer and guitarist. You can find _some_ of my photography [here](https://500px.com/gregsharpe1). 

For more information about myself, check out the [contact](/contact) page.
